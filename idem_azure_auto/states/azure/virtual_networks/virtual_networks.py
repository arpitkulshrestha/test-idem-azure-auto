"""
Autogenerated using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__


"""
import copy
from collections import OrderedDict
from typing import Any
from typing import Dict
from typing import List


__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    resource_group_name: str,
    virtual_network_name: str,
    address_space: List,
    location: str,
    resource_id: str = None,
    bgp_communities: Dict = None,
    flow_timeout_in_minutes: int = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Create or update Virtual Networks

    Args:
        name(str): The identifier for this state.
        resource_group_name(str): The name of the resource group.
        virtual_network_name(str): The name of the virtual network.
        address_space(list): An array of IP address ranges that can be used by subnets of the virtual network.
        location(str): Resource location. This field can not be updated.
        resource_id(str, optional): Virtual Network resource id on Azure
        bgp_communities(dict, optional): Bgp Communities sent over ExpressRoute with each route corresponding to a prefix in this VNET.
        flow_timeout_in_minutes(int, optional): The FlowTimeout value (in minutes) for the Virtual Network
        tags(dict, optional): Resource tags.

    Returns:
        dict

    Examples:

        .. code-block:: sls

            my-vnet:
              azure.virtual_networks.virtual_networks.present:
                - name: my-vnet
                - resource_group_name: my-rg-1
                - virtual_network_name: my-vnet-1
                - location: westus
                - flow_timeout_in_minutes: 15
                - tags:
                    my-tag-key: my-tag-value
                - address_space:
                    - 10.0.0.0/26
    """
    result = {
        "name": name,
        "result": True,
        "old_state": None,
        "new_state": None,
        "comment": [],
    }
    subscription_id = ctx.acct.subscription_id
    if resource_id is None:
        resource_id = f"/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}"
    response_get = await hub.exec.request.json.get(
        ctx,
        url=f"{hub.exec.azure.URL}{resource_id}?api-version=2021-03-01",
        success_codes=[200],
    )

    if not response_get["result"]:
        if response_get["status"] == 404:
            if ctx.get("test", False):
                # Return a proposed state by Idem state --test
                result[
                    "new_state"
                ] = hub.tool.azure.test_state_utils.generate_test_state(
                    enforced_state={},
                    desired_state={
                        "name": name,
                        "resource_group_name": resource_group_name,
                        "virtual_network_name": virtual_network_name,
                        "address_space": address_space,
                        "tags": tags,
                        "location": location,
                        "resource_id": resource_id,
                        "flow_timeout_in_minutes": flow_timeout_in_minutes,
                        "bgp_communities": bgp_communities,
                    },
                )
                result["comment"].append(
                    f"Would create azure.virtual_networks.virtual_networks '{name}'"
                )
                return result
            else:
                # PUT operation to create a resource
                payload = hub.tool.azure.virtual_networks.conversion_utils.convert_present_to_raw_virtual_network(
                    address_space=address_space,
                    location=location,
                    bgp_communities=bgp_communities,
                    flow_timeout_in_minutes=flow_timeout_in_minutes,
                    tags=tags,
                )
                response_put = await hub.exec.request.json.put(
                    ctx,
                    url=f"{hub.exec.azure.URL}{resource_id}?api-version=2021-03-01",
                    success_codes=[200, 201],
                    json=payload,
                )

                if not response_put["result"]:
                    hub.log.debug(
                        f"Could not create azure.virtual_networks.virtual_networks {response_put['comment']} {response_put['ret']}"
                    )
                    result["comment"] = [response_put["comment"], response_put["ret"]]
                    result["result"] = False
                    return result

                result[
                    "new_state"
                ] = hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present(
                    resource=response_put["ret"],
                    idem_resource_name=name,
                    resource_group_name=resource_group_name,
                    virtual_network_name=virtual_network_name,
                    resource_id=resource_id,
                )
                result["comment"].append(
                    f"Created azure.virtual_networks.virtual_networks '{name}'"
                )
                return result

        else:
            hub.log.debug(
                f"Could not get azure.virtual_networks.virtual_networks {response_get['comment']} {response_get['ret']}"
            )
            result["result"] = False
            result["comment"] = [response_get["comment"], response_get["ret"]]
            return result
    else:
        existing_resource = response_get["ret"]
        result[
            "old_state"
        ] = hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present(
            resource=existing_resource,
            idem_resource_name=name,
            resource_group_name=resource_group_name,
            virtual_network_name=virtual_network_name,
            resource_id=resource_id,
        )
        # Generate a new PUT operation payload with new values
        new_payload = hub.exec.azure.virtual_networks.virtual_networks.update_virtual_network_payload(
            response_get["ret"],
            {
                "address_space": address_space,
                "bgp_communities": bgp_communities,
                "flow_timeout_in_minutes": flow_timeout_in_minutes,
                "tags": tags,
            },
        )
        if ctx.get("test", False):
            if new_payload["ret"] is None:
                result["new_state"] = copy.deepcopy(result["old_state"])
                result["comment"].append(
                    f"azure.virtual_networks.virtual_networks '{name}' has no property need to be updated."
                )
            else:
                result[
                    "new_state"
                ] = hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present(
                    resource=new_payload["ret"],
                    idem_resource_name=name,
                    resource_group_name=resource_group_name,
                    virtual_network_name=virtual_network_name,
                    resource_id=resource_id,
                )
                result["comment"].append(
                    f"Would update azure.virtual_networks.virtual_networks '{name}'"
                )
            return result
        # PUT operation to update a resource
        if new_payload["ret"] is None:
            result["new_state"] = copy.deepcopy(result["old_state"])
            result["comment"].append(
                f"azure.virtual_networks.virtual_networks '{name}' has no property need to be updated."
            )
            return result
        result["comment"] += new_payload["comment"]
        response_put = await hub.exec.request.json.put(
            ctx,
            url=f"{hub.exec.azure.URL}{resource_id}?api-version=2021-03-01",
            success_codes=[200],
            json=new_payload["ret"],
        )

        if not response_put["result"]:
            hub.log.debug(
                f"Could not update azure.virtual_networks.virtual_networks {response_put['comment']} {response_put['ret']}"
            )
            result["result"] = False
            result["comment"] = [response_get["comment"], response_get["ret"]]
            return result

        result[
            "new_state"
        ] = hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present(
            resource=response_put["ret"],
            idem_resource_name=name,
            resource_group_name=resource_group_name,
            virtual_network_name=virtual_network_name,
            resource_id=resource_id,
        )
        result["comment"].append(
            f"Updated azure.virtual_networks.virtual_networks '{name}'"
        )
        return result


async def absent(
    hub, ctx, name: str, resource_group_name: str, virtual_network_name: str
) -> Dict:
    r"""
    **Autogenerated function**

    Delete Virtual Networks

    Args:
        name(str): The identifier for this state.
        resource_group_name(str): The name of the resource group.
        virtual_network_name(str): The name of the virtual network.

    Returns:
        dict

    Examples:

        .. code-block:: sls

            resource_is_absent:
              azure.virtual_networks.virtual_networks.absent:
                - name: value
                - resource_group_name: value
                - virtual_network_name: value
    """
    result = dict(name=name, result=True, comment=[], old_state=None, new_state=None)
    subscription_id = ctx.acct.subscription_id
    resource_id = f"/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}"
    response_get = await hub.exec.request.json.get(
        ctx,
        url=f"{hub.exec.azure.URL}{resource_id}?api-version=2021-03-01",
        success_codes=[200],
    )
    if response_get["result"]:
        result[
            "old_state"
        ] = hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present(
            resource=response_get["ret"],
            idem_resource_name=name,
            resource_group_name=resource_group_name,
            virtual_network_name=virtual_network_name,
            resource_id=resource_id,
        )
        if ctx.get("test", False):
            result["comment"].append(
                f"Would delete azure.virtual_networks.virtual_networks '{name}'"
            )
            return result
        response_delete = await hub.exec.request.raw.delete(
            ctx,
            url=f"{hub.exec.azure.URL}{resource_id}?api-version=2021-03-01",
            success_codes=[200, 202, 204],
        )

        if not response_delete["result"]:
            hub.log.debug(
                f"Could not delete azure.virtual_networks.virtual_networks '{name}' {response_delete['comment']} {response_delete['ret']}"
            )
            result["result"] = False
            result["comment"] = [response_delete["comment"], response_delete["ret"]]
            return result

        result["comment"].append(
            f"Deleted azure.virtual_networks.virtual_networks '{name}'"
        )
        return result
    elif response_get["status"] == 404:
        # If Azure returns 'Not Found' error, it means the resource has been absent.
        result["comment"].append(
            f"azure.virtual_networks.virtual_networks '{name}' already absent"
        )
        return result
    else:
        hub.log.debug(
            f"Could not get azure.virtual_networks.virtual_networks '{name}' {response_get['comment']} {response_get['ret']}"
        )
        result["result"] = False
        result["comment"] = [response_get["comment"], response_get["ret"]]
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    r"""
    **Autogenerated function**

    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    List all Virtual Networks under the same subscription


    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: bash

            $ idem describe azure.virtual_networks.virtual_networks
    """

    result = {}
    subscription_id = ctx.acct.subscription_id
    uri_parameters = OrderedDict(
        {
            "resourceGroups": "resource_group_name",
            "virtualNetworks": "virtual_network_name",
        }
    )
    async for page_result in hub.tool.azure.request.paginate(
        ctx,
        url=f"{hub.exec.azure.URL}/subscriptions/{subscription_id}/providers/Microsoft.Network/virtualNetworks?api-version=2021-03-01",
        success_codes=[200],
    ):
        resource_list = page_result.get("value")
        if resource_list:
            for resource in resource_list:
                resource_id = resource["id"]
                uri_parameter_values = hub.tool.azure.uri.get_parameter_value_in_dict(
                    resource_id, uri_parameters
                )
                resource_translated = hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present(
                    resource=resource,
                    idem_resource_name=resource_id,
                    resource_id=resource_id,
                    **uri_parameter_values,
                )
                result[resource_id] = {
                    f"azure.virtual_networks.virtual_networks.present": [
                        {parameter_key: parameter_value}
                        for parameter_key, parameter_value in resource_translated.items()
                    ]
                }
    return result
