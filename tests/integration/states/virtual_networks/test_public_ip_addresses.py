import uuid

import pytest


@pytest.mark.asyncio
async def test_public_ip_addresses(hub, ctx, resource_group_fixture):
    """
    This test provisions a public ip address, describes public ip addresses and deletes
     the provisioned public ip address.
    """
    # Create public ip address
    resource_group_name = resource_group_fixture.get("name")
    public_ip_address_name = "idem-test-pub-ip-" + str(uuid.uuid4())
    pub_ip_parameters = {
        "location": "eastus",
        "properties": {
            "publicIPAllocationMethod": "Static",
            "idleTimeoutInMinutes": 10,
            "publicIPAddressVersion": "IPv4",
        },
    }
    pub_ip_ret = await hub.states.azure.virtual_networks.public_ip_addresses.present(
        ctx,
        name=public_ip_address_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        parameters=pub_ip_parameters,
    )
    assert pub_ip_ret["result"], pub_ip_ret["comment"]
    assert not pub_ip_ret["changes"].get("old") and pub_ip_ret["changes"]["new"]

    pub_ip_wait_ret = await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/publicIPAddresses/{public_ip_address_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
    hub.tool.azure.resource.check_response_payload(
        pub_ip_parameters, pub_ip_wait_ret["ret"]
    )

    # Describe public ip address
    describe_ret = await hub.states.azure.virtual_networks.public_ip_addresses.describe(
        ctx
    )
    assert pub_ip_wait_ret["ret"].get("id") in describe_ret

    # Delete public ip address
    del_ret = await hub.states.azure.virtual_networks.public_ip_addresses.absent(
        ctx,
        name=public_ip_address_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
    )
    assert del_ret["result"], del_ret["comment"]
    assert del_ret["changes"]["old"] and not del_ret["changes"].get("new")
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/publicIPAddresses/{public_ip_address_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
