import dict_tools.differ as differ
import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
NETWORK_SECURITY_GROUP_NAME = "my-security-group"
RESOURCE_PARAMETERS = {"location": "eastus"}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of network security groups. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.virtual_networks.network_security_groups.present = (
        hub.states.azure.virtual_networks.network_security_groups.present
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.virtual_networks.network_security_groups",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert json == RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.virtual_networks.network_security_groups.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        RESOURCE_PARAMETERS,
    )
    assert differ.deep_diff(dict(), expected_put.get("ret")) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_put.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of network security groups. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.virtual_networks.network_security_groups.present = (
        hub.states.azure.virtual_networks.network_security_groups.present
    )
    patch_parameter = {"tags": {"new-tag-key": "new-tag-value"}}

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_patch = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
            "tags": {"new-tag-key": "new-tag-value"},
        },
        "result": True,
        "status": 200,
        "comment": f"Would update azure.virtual_networks.network_security_groups with parameters: {patch_parameter}.",
    }

    def _check_patch_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert json == patch_parameter
        return expected_patch

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.json.patch.side_effect = _check_patch_parameters
    mock_hub.tool.azure.request.patch_json_content.return_value = patch_parameter

    ret = await mock_hub.states.azure.virtual_networks.network_security_groups.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        patch_parameter,
    )
    assert differ.deep_diff(
        expected_get.get("ret"), expected_patch.get("ret")
    ) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_patch.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of network security groups. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.virtual_networks.network_security_groups.absent = (
        hub.states.azure.virtual_networks.network_security_groups.absent
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.virtual_networks.network_security_groups.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NETWORK_SECURITY_GROUP_NAME
    )
    assert not ret["changes"]
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert f"'{RESOURCE_NAME}' already absent" == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of network security groups. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.virtual_networks.network_security_groups.absent = (
        hub.states.azure.virtual_networks.network_security_groups.absent
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.virtual_networks.network_security_groups.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NETWORK_SECURITY_GROUP_NAME
    )
    assert differ.deep_diff(expected_get.get("ret"), dict()) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_delete.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of network security groups.
    """
    mock_hub.states.azure.virtual_networks.network_security_groups.describe = (
        hub.states.azure.virtual_networks.network_security_groups.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value = hub.tool.azure.uri.get_parameter_value
    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [{"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS}]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.virtual_networks.network_security_groups.describe(
        ctx
    )

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.virtual_networks.network_security_groups.present" in ret_value.keys()
    expected_describe = [
        {"resource_group_name": RESOURCE_GROUP_NAME},
        {"network_security_group_name": NETWORK_SECURITY_GROUP_NAME},
        {"parameters": expected_list.get("ret").get("value")[0]},
    ]
    assert expected_describe == ret_value.get(
        "azure.virtual_networks.network_security_groups.present"
    )
