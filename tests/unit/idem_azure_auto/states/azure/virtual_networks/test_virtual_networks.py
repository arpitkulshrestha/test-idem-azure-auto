import copy
from collections import ChainMap

import dict_tools.differ as differ
import pytest

RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
VIRTUAL_NETWORK_NAME = "my-vnet"
RESOURCE_PARAMETERS = {
    "address_space": ["10.12.13.0/25"],
    "location": "eastus",
    "flow_timeout_in_minutes": 10,
    "tags": {"tag-key": "tag-value"},
}
RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "addressSpace": {"addressPrefixes": ["10.12.13.0/25"]},
        "flowTimeoutInMinutes": 10,
    },
    "location": "eastus",
    "tags": {"tag-key": "tag-value"},
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of virtual networks. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.virtual_networks.virtual_networks.present = (
        hub.states.azure.virtual_networks.virtual_networks.present
    )
    mock_hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present = (
        hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present
    )
    mock_hub.tool.azure.virtual_networks.conversion_utils.convert_present_to_raw_virtual_network = (
        hub.tool.azure.virtual_networks.conversion_utils.convert_present_to_raw_virtual_network
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        assert not differ.deep_diff(RESOURCE_PARAMETERS_RAW, json)
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.virtual_networks.virtual_networks.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        VIRTUAL_NETWORK_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.virtual_networks.virtual_networks '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.virtual_networks.virtual_networks.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        VIRTUAL_NETWORK_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.virtual_networks.virtual_networks '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of virtual networks. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.virtual_networks.virtual_networks.present = (
        hub.states.azure.virtual_networks.virtual_networks.present
    )
    mock_hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present = (
        hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present
    )
    mock_hub.exec.azure.virtual_networks.virtual_networks.update_virtual_network_payload = (
        hub.exec.azure.virtual_networks.virtual_networks.update_virtual_network_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {
        "address_space": ["10.12.13.0/26"],
        "location": "eastus",
        "flow_timeout_in_minutes": 15,
        "tags": {"tag-new-key": "tag-new-value"},
    }

    resource_parameters_update_raw = {
        "properties": {
            "addressSpace": {"addressPrefixes": ["10.12.13.0/26"]},
            "flowTimeoutInMinutes": 15,
        },
        "location": "eastus",
        "tags": {"tag-new-key": "tag-new-value"},
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        assert resource_parameters_update_raw["location"] == json.get("location")
        assert resource_parameters_update_raw["tags"] == json.get("tags")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.virtual_networks.virtual_networks.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        VIRTUAL_NETWORK_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.virtual_networks.virtual_networks '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.virtual_networks.virtual_networks.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        VIRTUAL_NETWORK_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of virtual networks. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.virtual_networks.virtual_networks.absent = (
        hub.states.azure.virtual_networks.virtual_networks.absent
    )
    mock_hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present = (
        hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.virtual_networks.virtual_networks.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VIRTUAL_NETWORK_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.virtual_networks.virtual_networks '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of virtual networks. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.virtual_networks.virtual_networks.absent = (
        hub.states.azure.virtual_networks.virtual_networks.absent
    )
    mock_hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present = (
        hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.virtual_networks.virtual_networks.absent(
        test_ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VIRTUAL_NETWORK_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.virtual_networks.virtual_networks '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.virtual_networks.virtual_networks.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VIRTUAL_NETWORK_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.virtual_networks.virtual_networks '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of virtual networks.
    """
    mock_hub.states.azure.virtual_networks.virtual_networks.describe = (
        hub.states.azure.virtual_networks.virtual_networks.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present = (
        hub.tool.azure.virtual_networks.conversion_utils.convert_raw_virtual_network_to_present
    )

    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.virtual_networks.virtual_networks.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.virtual_networks.virtual_networks.present" in ret_value.keys()
    described_resource = ret_value.get(
        "azure.virtual_networks.virtual_networks.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert VIRTUAL_NETWORK_NAME == old_state.get("virtual_network_name")
        assert expected_old_state["address_space"] == old_state.get("address_space")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["flow_timeout_in_minutes"] == old_state.get(
            "flow_timeout_in_minutes"
        )
        assert expected_old_state["tags"] == old_state.get("tags")
    if new_state:
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert VIRTUAL_NETWORK_NAME == new_state.get("virtual_network_name")
        assert expected_new_state["address_space"] == new_state.get("address_space")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["flow_timeout_in_minutes"] == new_state.get(
            "flow_timeout_in_minutes"
        )
        assert expected_new_state["tags"] == new_state.get("tags")
