import dict_tools.differ as differ
import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
ROUTE_TABLE_NAME = "my-route-table"
ROUTE_NAME = "my-route"
RESOURCE_PARAMETERS = {
    "properties": {
        "addressPrefix": "10.0.3.0/24",
        "nextHopType": "VirtualNetworkGateway",
    }
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of routes. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.virtual_networks.routes.present = (
        hub.states.azure.virtual_networks.routes.present
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/routeTables/{ROUTE_TABLE_NAME}/routes/{ROUTE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.virtual_networks.routes",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ROUTE_TABLE_NAME in url
        assert ROUTE_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ROUTE_TABLE_NAME in url
        assert ROUTE_NAME in url
        assert json == RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.virtual_networks.routes.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        ROUTE_TABLE_NAME,
        ROUTE_NAME,
        RESOURCE_PARAMETERS,
    )
    assert differ.deep_diff(dict(), expected_put.get("ret")) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_put.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of routes. When a resource exists, 'present' will return the existing resource. Since Azure
     subnets doesn't support PATCH operation
    """
    mock_hub.states.azure.virtual_networks.routes.present = (
        hub.states.azure.virtual_networks.routes.present
    )
    # This patch parameter should be ignored by "present"
    patch_parameter = {"tags": {"new-tag-key": "new-tag-value"}}

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/routeTables/{ROUTE_TABLE_NAME}/routes/{ROUTE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_get

    ret = await mock_hub.states.azure.virtual_networks.routes.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        ROUTE_TABLE_NAME,
        ROUTE_NAME,
        patch_parameter,
    )
    assert differ.deep_diff(
        expected_get.get("ret"), expected_get.get("ret")
    ) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_get.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of routes. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.virtual_networks.routes.absent = (
        hub.states.azure.virtual_networks.routes.absent
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ROUTE_TABLE_NAME in url
        assert ROUTE_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.virtual_networks.routes.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, ROUTE_TABLE_NAME, ROUTE_NAME
    )
    assert not ret["changes"]
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert f"'{RESOURCE_NAME}' already absent" == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of routes. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.virtual_networks.routes.absent = (
        hub.states.azure.virtual_networks.routes.absent
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/routeTables/{ROUTE_TABLE_NAME}/routes/{ROUTE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ROUTE_TABLE_NAME in url
        assert ROUTE_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.virtual_networks.routes.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, ROUTE_TABLE_NAME, ROUTE_NAME
    )
    assert differ.deep_diff(expected_get.get("ret"), dict()) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_delete.get("comment") == ret.get("comment")


# TODO: add describe unit test
